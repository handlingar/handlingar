#!/bin/bash

# Requirements:
# find, pg_dump and rsync installed and in path for user that runs the script
# /usr/lib/sendmail
# 
# Installation:
# Make this file executable:
# `sudo chmod u+x /home/USER/backup.sh`
# 
# Create and setup /home/USER/.pgpass for authentication,
# see https://www.postgresql.org/docs/current/libpq-pgpass.html
#
# Setup ssh keys and ssh config to connect to the backup server.

LC_CTYPE=en_US.UTF-8
LC_ALL=en_US.UTF-8

BACKUP_DIR=$HOME/backups
DAYS_TO_KEEP=30
FILE_SUFFIX=_pg_backup.sql
DATABASE=
DB_USER=

BACKUP_FILENAME=`date +"%Y%m%d%H%M"`${FILE_SUFFIX}

OUTPUT_FILEPATH=${BACKUP_DIR}/${BACKUP_FILENAME}

BACKUP_SERVER_PATH=
BACKUP_SERVER_USER=
BACKUP_SERVER_HOSTNAME=

# Where the raw emails that should be backed up are stored on the server:
FILES_LOCATION="/var/www/alaveteli/files"

NOTIFICATION_EMAIL=

LOGFILE=$BACKUP_DIR/backup.log


# Log file
function log {
    echo -n `date +"%F %T: "` >> $LOGFILE
    echo " "$1 >> $LOGFILE
}

# Emails backup notifcations to email address specified in NOTIFICATION_EMAIL
# Takes 2 arguments: Subject message and Message
function email {
/usr/lib/sendmail -v ${NOTIFICATION_EMAIL}<<EOF
subject: Backup Notice - "$1"  $(date) for $(hostname)
"$2" @ $(date) for $(hostname)
EOF
} 

# Create backup dir if it does not exist
mkdir -p ${BACKUP_DIR}

# Backup a Postgresql database into a daily file.
pg_dump -U ${DB_USER} -h localhost ${DATABASE} -F p -f ${OUTPUT_FILEPATH} >> ${LOGFILE}
if ! [ $? -eq 0 ]
then
    email "pg_dump failed" "pg_dump failed"
fi

# Move backup file to backup server
rsync -zarvh -e "ssh -o StrictHostKeyChecking=no" ${OUTPUT_FILEPATH} ${BACKUP_SERVER_USER}@${BACKUP_SERVER_HOSTNAME}:${BACKUP_SERVER_PATH} >> ${LOGFILE}
if ! [ $? -eq 0 ]
then
    email "rsync failed to copy backup" "rsync failed to copy backup file"
fi

# Move raw emails to backup server
rsync -zarvh -e "ssh -o StrictHostKeyChecking=no" ${FILES_LOCATION} ${BACKUP_SERVER_USER}@${BACKUP_SERVER_HOSTNAME}:${BACKUP_SERVER_PATH} >> ${LOGFILE}
if ! [ $? -eq 0 ]
then
    email "rsync failed to copy raw emails" "rsync failed to copy raw emails"
fi

# prune old backups
find ${BACKUP_DIR} -maxdepth 1 -mtime +${DAYS_TO_KEEP} -name "*${FILE_SUFFIX}" -exec rm -rf '{}' ';' >> ${LOGFILE}
if ! [ $? -eq 0 ]
then
    email "prune old backups failed" "prune old backups failed"
fi
