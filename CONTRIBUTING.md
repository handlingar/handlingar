# Contributor Guidelines (Riktlinjer för Bidragsgivare)

## Advice for new contributors (Advice for new contributors)

Start small. The PRs that have the best chance of being accepted are those that make small changes that are easy to verify, with clear and specific intentions. See below for more [guidelines on amendments](#pull-requests).

It is a good idea to put your interest in the development work by finding the existing (per project, see below) case for it or by creating a new case yourself. You can also use that ticket as a place to signal your intentions and get feedback from the users who are most likely to appreciate your proposed changes.

Once you have spent some of your time planning your solution, it is good to go back to the ticket and describe your proposed solution to the change. We are happy to provide feedback and discuss. [An
ounce of prevention, as they say!](https://www.goodreads.com/quotes/247269-an-ounce-of-prevention-is-worth-a-pound-of-cure)

### Contribute to content - without code and through discussion

#### Handlingar.se (this project repo)
Look at [case](https://gitlab.com/handlingar/handlingar/-/issues) and discuss. Add new case if needed.

#### Handlingar.se Theme (GitHub)

Look at [case](https://github.com/handlingar/handlingar-theme/issues) and discuss. Add new case if needed.

#### Alaveteli.org (open source software)

Look at [case](https://github.com/mysociety/alaveteli/issues) and discuss. Add a new issue if needed.

### Contribute to code - Install Alaveteli to start testing your amendments.

Follow this [guide](https://alaveteli.org/docs/installing/). Docker is recommended to start testing whose guide can be found [here](https://alaveteli.org/docs/installing/docker/). Manual installation is recommended to test code and its guide can be found [here](https://alaveteli.org/docs/installing/manual_install/). 

## Pull requests (Amendments)

Do you want to create an amendment in Alaveteli? Please observe the following guidelines. 

- First, make sure your run passes and shows the latest version you downloaded.
- Run [rebase](https://nathanleclaire.com/blog/2014/09/14/dont-be-scared-of-git-rebase/) 
for your changes on the latest [`develop` branch](https://github.com/mysociety/alaveteli/tree/develop) and resolve any conflicts.
  This ensures that your changes will merge easily when you open your amendment (PR).
- Make sure everything works and run tests!
- Make sure that the diff between our main branch and your branch contains only the minimum amount of changes needed to implement your feature or bug fix. This will make it easier for the person reviewing your code to approve the changes. Please do not submit a proposal for change (PR) with commented out code or unfinished features.
- Avoid meaningless or too granular contributions. If your branch contains contributions with lines like "Oops, reverted this change" or "Just experimenting, will delete later", please [mash/squash or rebase these changes](https://robots.thoughtbot.com/git-interactive-rebase-squash-amend-rewriting-history).
- Don't have too few attachments. If you have a complicated or major amendment on your branch, it may be sensible to break those amendments into logical atomic pieces to aid the review process.
- Provide a well-written and nicely formatted submission notice. See [this link](http://chris.beams.io/posts/git-commit/)
  for some tips on formatting. As far as content goes, try to include in your abstract:
  1.  What you changed
  2.  Why this change was made (including the git issue # if possible)
  3.  Any relevant technical details or motivations for your implementation choices that might be helpful to someone reviewing or revising the history of contributions in the future. When in doubt, run err on the side of a longer commit message.

Above all, spend some time with the code repository. Follow our (upcoming) template for pull requests that can be added to your amendment automatically. Take a look at recently introduced amendments and see how they did things.

# Riktlinjer för Bidragsgivare (Contributor Guidelines)

## Råd till nya bidragsgivare (Advice for new contributors)

Börja litet. De ändringsförlsag (PR) som har störtst chans att komma med är de som gör små ändringar som är enkla att verifiera, med tydliga och specifika intentioner. Se nedan för mer [riktlinjer om ändringsförslag](#pull-requests).

Det är en bra idé att omsätta ditt intresse i utvecklingsarbetet genom att hitta det som existerande (per projekt, se nedan) för det eller genom att skapa ett nytt ärende själv. Du kan också använda det ärendet som en plats för att signalera dina intentioner och få feedback från de användare som är mest troliga att uppskatta dina ändringsförslag.

När du spenderat lite av din tid på att planera din lösning så är det bra att gå tillbaka till ärendet och beskriva ditt lösningsförslag till ändringen. Vi lämnar gärna feedback och diskuterar. [An
ounce of prevention, as they say!](https://www.goodreads.com/quotes/247269-an-ounce-of-prevention-is-worth-a-pound-of-cure)

### Bidra till innehåll - utan kod och genom diskussion

#### Handlingar.se (detta projekt)
Titta på [ärende](https://gitlab.com/handlingar/handlingar/-/issues) och diskutera. Lägg till nytt ärende vid behov.

#### Handlingar.se Tema (GitHub)

Titta på [ärende](https://github.com/handlingar/handlingar-theme/issues) och diskutera. Lägg till nytt ärende vid behov.

#### Alaveteli.org (open source software)

Titta på [ärende](https://github.com/mysociety/alaveteli/issues) och diskutera. Lägg till nytt ärende vid behov.

### Bidra till kod - Installera Alaveteli för att börja testa dina ändringsförslag

Följ denna [guide](https://alaveteli.org/docs/installing/). Docker rekommenderas för att börja testa vars guide finns [här](https://alaveteli.org/docs/installing/docker/). Manuell installation rekommenderas för att testa kod och dess guide finns [här](https://alaveteli.org/docs/installing/manual_install/). 

## Pull requests (Ändringsförslag)

Vill du skapa ett ändringsförslag i Alaveteli? Vänligen observera följande riktlinjer. 

- Först, se till att din körning passerar och visar senaste versionen du hämtat hem.
- Kör [rebase](https://nathanleclaire.com/blog/2014/09/14/dont-be-scared-of-git-rebase/) 
för dina ändringar på den senaste [`develop`-grenen](https://github.com/mysociety/alaveteli/tree/develop) och lös eventuella konflikter.
  Detta försäkrar dig om att dina förändringar kommer sammanföras enkelt när du öppnar ditt ändringsförslag (PR).
- Försäkra dig om att allt funkar och kör test!
- Försäkra dig om att diffen mellan vår main-gren och din gren endast innehåller de minimala mängden av behövda förändringar för att implementera din funktion eller buggfix. Detta kommer göra det enklare för personen som granskar din kod för att godkänna förändringarna. Vänligen skicka inte in ett ändringsförslag (PR) med utkommenterad kod eller oavslutade funktionre.
- Undvik meningslösa eller för granulära bidrag. Om din gren innehåller bidrag med rader som "Oops, återställde denna förändring" eller "Experimenterar bara, kommer radera senare", vänligen [mosa/squasha eller rebase:a bort dessa ändringar](https://robots.thoughtbot.com/git-interactive-rebase-squash-amend-rewriting-history).
- Ha inte för få birag. Om du har en komplicerad eller större ändringsförslag på din gren så kan det vara förnuftigt att bryta upp de ändringarna i logiska atomiska bitar för att stötta i granskningsprocessen.
- Tillhandahåll en välskriven och trevligt formaterat bidragsmeddelande. Se [denna länk](http://chris.beams.io/posts/git-commit/)
  för några tips på formattering. Så lång som innehåll går, försöka inkludera i din sammanfattning:
  1.  Vad du ändrat
  2.  Varför denna ändring gjordes (inklusive git-ärendets # om möjligt)
  3.  Några relevanta tekniska detaljer eller motivationer för dina val vid implementation som kan vara hjälpsamma för någon som granskar eller reviderar historiken över bidrag i framtiden. När du tvivlar, kör err på sidan av ett längre commit-meddelnade.

Spendera framförallt lite tid med kodförrådet. Följ vår (kommande) mall för pull requests som kan läggas till för ditt ändringsförslag automatiskt. Ta en koll på nyligen införda ändringsförslag och titta på hur de gjorde saker.
