# Handlingar.se (English)

We want to give everyone access to tax-funded information in a digital and secure way. Handlingar.se makes it easy for people to search, find and enquire about public documents and public data financed by your tax bill. We want to make information available for better decisions in society. All people have the right to access information held by Swedish authorities. By law, Swedish authorities, regions and municipalities must respond.

Please give us a star ⭐ if you support our development for a digitalized and stronger Freedom of Information!

## Getting Started (README is work in progress)

These instructions will get you a copy of the Alaveteli project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them:

```
Check these instructions [here](https://alaveteli.org/docs/installing/)
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc


# Handlingar.se (Swedish)

Vi vill ge alla tillgång till skattefinansierad information på digitalt och säkert sätt. Handlingar.se gör det lätt för människor att söka, hitta och fråga om allmänna handlingar och allmänna data som finansieras med din skattesedel. Vi vill tillgängliggöra information för bättre beslut i samhället. Alla människor har rätt att få tillgång till information som svenska myndigheter har. Enligt lag måste svenska myndigheter, regioner och kommuner svara.

Ge oss gärna en stjärna ⭐ om du stödjer vår utveckling för en digitaliserad och starkare offentlighetsprincip!

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

# Handbok

Kolla in vår projekthandbok på [wikin](https://gitlab.com/handlingar/handlingar/-/wikis/) där du kan läsa mer om hur projektet drivs och lär dig hur du kan bli involverad!

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc


# Welcome to the project Handlingar.se.

Please give us a ⭐ if you support a development to a digitized and stronger Freedom of Information legislature (Principle for Public Access - offentlighetsprincipen)!

# Handbook

Check out our project Handbook on the [wiki](https://gitlab.com/handlingar/handlingar/-/wikis/) where you can read more on how we operate the project and learn how to get involved!



